#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/moment_of_inertia_estimation.h>

#include <vector>
#include <string>

/***
 * Get the point cloud from ply mesh.
 *
 * */
void getPointcloudFromPly(std::string mesh_file_name, pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud,
                          std::string pcd_file_name)
{
    pcl::PCLPointCloud2 cloud_pcl2;
    pcl::io::loadPLYFile (mesh_file_name, cloud_pcl2);
     
    // Convert to pcl pointcloud:
    pcl::fromPCLPointCloud2 (cloud_pcl2, *cloud);
    pcl::io::savePCDFile(pcd_file_name, *cloud, true);

    return;
}

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cout << "Usage: inertia_estimation input.ply output.pcd" << std::endl;
    }
    std::string input_mesh_file = std::string(argv[1]);
    std::string output_pcd_file = std::string(argv[2]);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    getPointcloudFromPly(input_mesh_file, cloud, output_pcd_file);

    pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;
    feature_extractor.setInputCloud (cloud);
    feature_extractor.compute ();
    std::vector <float> moment_of_inertia;
    feature_extractor.getMomentOfInertia (moment_of_inertia);

    return 1;
}
