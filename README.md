### What is this repository for? ###
This is the grasp pipeline, which is used for both multi-fingered grasp data collection and experiments of our ISRR 2017 grasp planning paper.

### Branches ###
sim_grasp_experiments branch is for simulation experiments. master branch is the same with sim_grasp_experiments right now. 
real_robot_grasp_experiments branch is for real robot experiments. 

### ROS package name ###
grasp_data_collection_pkg

### Dependencies ###
Tensorflow 1.1.0
Opencv 2.4.8
PCL 1.7
ROS Indigo  
Our object segmentation package  
Our blensor package (changed based on blensor)
MoveIt!  
Our robot controller
Our urlg_robot_gazebo package.

### How to run the experiments pipeline ###
roslaunch urlg_robots_gazebo lbr4_allegro_control_position.launch  
roslaunch lbr4_moveit moveit_lbr4_allegro.launch  
roslaunch grasp_data_collection_pkg data_collection_servers.launch    
rosrun grasp_data_collection_pkg launch_blensor_server.py  
roslaunch grasp_data_collection_pkg data_collection_client.launch  
roslaunch grasps_detection_cnn_gd_pkg grasps_inference.launch  


