#!/usr/bin/env python

import roslib; roslib.load_manifest('grasp_data_collection_pkg')
import rospy
from grasp_data_collection_pkg.srv import *
from geometry_msgs.msg import Pose, Quaternion
from sensor_msgs.msg import JointState, CameraInfo
import tf
import numpy as np
import h5py
from compute_finger_tip_location import ComputeFingerTipPose

class RecordGraspData():
    def __init__(self):
        rospy.init_node('record_grasp_data_server')
        self.num_grasps_per_object = rospy.get_param('~num_grasps_per_object', 10)
        self.data_recording_path = rospy.get_param('~data_recording_path', '/media/kai/multi_finger_grasp_data/')
        self.use_hd = rospy.get_param('~use_hd', True)
        self.grasp_file_name = self.data_recording_path + 'grasp_data.h5'
        self.initialize_data_file()
        self.grasp_inits_inf_num = 4
        
        #if self.use_hd:
        #    self.camera_info_topic = rospy.get_param('~camera_info_topic', '/kinect2/hd/camera_info')
        #else:
        #    self.camera_info_topic = rospy.get_param('~camera_info_topic', '/kinect2/sd/camera_info')
        #self.camera_info = CameraInfo()
        #rospy.Subscriber(self.camera_info_topic, CameraInfo, self.get_camera_info)
        self.compute_finger_tip_loc = ComputeFingerTipPose()

    def initialize_data_file(self):
        #a: Read/write if exists, create otherwise (default)
        grasp_file = h5py.File(self.grasp_file_name, 'a')
        hand_joint_state_name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
        hand_js_name_key = 'hand_joint_state_name' 
        if hand_js_name_key not in grasp_file:
            grasp_file.create_dataset(hand_js_name_key, data=hand_joint_state_name)

        max_object_id_key = 'max_object_id'
        if max_object_id_key not in grasp_file:
            grasp_file.create_dataset(max_object_id_key, data=-1)
        cur_object_name_key = 'cur_object_name'
        if cur_object_name_key not in grasp_file:
            grasp_file.create_dataset(cur_object_name_key, data='empty')

        total_grasps_num_key = 'total_grasps_num'
        if total_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(total_grasps_num_key, data=0)
        suc_grasps_num_key = 'suc_grasps_num'
        if suc_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(suc_grasps_num_key, data=0)

        total_init_grasps_num_key = 'total_init_grasps_num'
        if total_init_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(total_init_grasps_num_key, data=0)
        suc_init_grasps_num_key = 'suc_init_grasps_num'
        if suc_init_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(suc_init_grasps_num_key, data=0)

        total_inf_grasps_num_key = 'total_inf_grasps_num'
        if total_inf_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(total_inf_grasps_num_key, data=0)
        suc_inf_grasps_num_key = 'suc_inf_grasps_num'
        if suc_inf_grasps_num_key not in grasp_file:
            grasp_file.create_dataset(suc_inf_grasps_num_key, data=0)
        
        total_init_suc_prob_key = 'total_init_suc_prob'
        if total_init_suc_prob_key not in grasp_file:
            grasp_file.create_dataset(total_init_suc_prob_key, data=0.)
        avg_init_suc_prob_key = 'avg_init_suc_prob'
        if avg_init_suc_prob_key not in grasp_file:
            grasp_file.create_dataset(avg_init_suc_prob_key, data=0.)

        total_inf_suc_prob_key = 'total_inf_suc_prob'
        if total_inf_suc_prob_key not in grasp_file:
            grasp_file.create_dataset(total_inf_suc_prob_key, data=0.)
        avg_inf_suc_prob_key = 'avg_inf_suc_prob'
        if avg_inf_suc_prob_key not in grasp_file:
            grasp_file.create_dataset(avg_inf_suc_prob_key, data=0.)

        grasp_file.close()
    
    def get_camera_info(self, camera_info):
        self.camera_info = camera_info
        #print self.camera_info

    def handle_record_grasp_data(self, req):
        if req.sample:
            self.grasp_inits_inf_num = 1
        #'r+': Read/write, file must exist
        grasp_file = h5py.File(self.grasp_file_name, 'r+')
        if req.grasp_id == 0:
            grasp_file['max_object_id'][()] +=1
            grasp_file['cur_object_name'][()] = req.object_name

        # [()] is the way to get the scalar value from h5 file.
        #object_id = grasp_file['max_object_id'][()] + 1 
        object_id = grasp_file['max_object_id'][()] 
        object_grasp_id = 'object_' + str(object_id) + '_grasp_' + str(req.grasp_id)

        object_init_grasps_num_key = 'object_' + str(object_id) +'_init_grasps_num'
        object_suc_init_grasps_num_key = 'object_' + str(object_id) +'_suc_init_grasps_num'
        object_inf_grasps_num_key = 'object_' + str(object_id) +'_inf_grasps_num'
        object_suc_inf_grasps_num_key = 'object_' + str(object_id) +'_suc_inf_grasps_num'

        object_total_init_suc_prob_key = 'object_' + str(object_id) +'_total_init_suc_prob'
        object_avg_init_suc_prob_key = 'object_' + str(object_id) +'_avg_init_suc_prob'
        object_total_inf_suc_prob_key = 'object_' + str(object_id) +'_total_inf_suc_prob'
        object_avg_inf_suc_prob_key = 'object_' + str(object_id) +'_avg_inf_suc_prob'

        #Inference success probability of successful and failing inference grasps
        object_total_suc_inf_suc_prob_key = 'object_' + str(object_id) + '_total_suc_inf_suc_prob'
        object_avg_suc_inf_suc_prob_key = 'object_' + str(object_id) + '_avg_suc_inf_suc_prob'
        object_total_fail_inf_suc_prob_key = 'object_' + str(object_id) + '_total_fail_inf_suc_prob'
        object_avg_fail_inf_suc_prob_key = 'object_' + str(object_id) + '_avg_fail_inf_suc_prob'

        if req.grasp_id == 0:
            if object_init_grasps_num_key not in grasp_file:
                grasp_file.create_dataset(object_init_grasps_num_key, data=0)
            if object_suc_init_grasps_num_key not in grasp_file:
                grasp_file.create_dataset(object_suc_init_grasps_num_key, data=0)

            if object_inf_grasps_num_key not in grasp_file:
                grasp_file.create_dataset(object_inf_grasps_num_key, data=0)
            if object_suc_inf_grasps_num_key not in grasp_file:
                grasp_file.create_dataset(object_suc_inf_grasps_num_key, data=0)

            if object_total_init_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_total_init_suc_prob_key, data=0.)
            if object_avg_init_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_avg_init_suc_prob_key, data=0.)

            if object_total_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_total_inf_suc_prob_key, data=0.)
            if object_avg_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_avg_inf_suc_prob_key, data=0.)

            if object_total_suc_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_total_suc_inf_suc_prob_key, data=0.)
            if object_avg_suc_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_avg_suc_inf_suc_prob_key, data=0.)

            if object_total_fail_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_total_fail_inf_suc_prob_key, data=0.)
            if object_avg_fail_inf_suc_prob_key not in grasp_file:
                grasp_file.create_dataset(object_avg_fail_inf_suc_prob_key, data=0.)


        grasp_object_name_key = 'object_' + str(object_id) + '_name'
        #print 'object_name:', req.object_name
        #print 'req.grasp_id:', req.grasp_id
        if grasp_object_name_key not in grasp_file:
            grasp_file.create_dataset(grasp_object_name_key, data=req.object_name)
    
        grasp_time_stamp_key = object_grasp_id + '_time_stamp'
        if grasp_time_stamp_key not in grasp_file:
            grasp_file.create_dataset(grasp_time_stamp_key, data=req.time_stamp)

        preshape_palm_pcd_pose_list = [req.preshape_palm_pcd_pose.pose.position.x, req.preshape_palm_pcd_pose.pose.position.y,
               req.preshape_palm_pcd_pose.pose.position.z, req.preshape_palm_pcd_pose.pose.orientation.x,
               req.preshape_palm_pcd_pose.pose.orientation.y, req.preshape_palm_pcd_pose.pose.orientation.z, 
               req.preshape_palm_pcd_pose.pose.orientation.w]
        palm_pcd_pose_key = object_grasp_id + '_preshape_palm_pcd_pose'
        if palm_pcd_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_pcd_pose_key, 
                    data=preshape_palm_pcd_pose_list)

        preshape_palm_world_pose_list = [req.preshape_palm_world_pose.pose.position.x, req.preshape_palm_world_pose.pose.position.y,
               req.preshape_palm_world_pose.pose.position.z, req.preshape_palm_world_pose.pose.orientation.x, 
               req.preshape_palm_world_pose.pose.orientation.y, req.preshape_palm_world_pose.pose.orientation.z, 
               req.preshape_palm_world_pose.pose.orientation.w]
        palm_world_pose_key = object_grasp_id + '_preshape_palm_world_pose'
        if palm_world_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_world_pose_key, 
                    data=preshape_palm_world_pose_list)

        true_preshape_palm_pcd_pose_list = [req.true_preshape_palm_pcd_pose.pose.position.x, req.true_preshape_palm_pcd_pose.pose.position.y,
               req.true_preshape_palm_pcd_pose.pose.position.z, req.true_preshape_palm_pcd_pose.pose.orientation.x,
               req.true_preshape_palm_pcd_pose.pose.orientation.y, req.true_preshape_palm_pcd_pose.pose.orientation.z, 
               req.true_preshape_palm_pcd_pose.pose.orientation.w]
        palm_pcd_pose_key = object_grasp_id + '_true_preshape_palm_pcd_pose'
        if palm_pcd_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_pcd_pose_key, 
                    data=true_preshape_palm_pcd_pose_list)

        true_preshape_palm_world_pose_list = [req.true_preshape_palm_world_pose.pose.position.x, req.true_preshape_palm_world_pose.pose.position.y,
               req.true_preshape_palm_world_pose.pose.position.z, req.true_preshape_palm_world_pose.pose.orientation.x, 
               req.true_preshape_palm_world_pose.pose.orientation.y, req.true_preshape_palm_world_pose.pose.orientation.z, 
               req.true_preshape_palm_world_pose.pose.orientation.w]
        palm_world_pose_key = object_grasp_id + '_true_preshape_palm_world_pose'
        if palm_world_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_world_pose_key, 
                    data=true_preshape_palm_world_pose_list)

        close_shape_palm_pcd_pose_list = [req.close_shape_palm_pcd_pose.pose.position.x, req.close_shape_palm_pcd_pose.pose.position.y,
               req.close_shape_palm_pcd_pose.pose.position.z, req.close_shape_palm_pcd_pose.pose.orientation.x,
               req.close_shape_palm_pcd_pose.pose.orientation.y, req.close_shape_palm_pcd_pose.pose.orientation.z, 
               req.close_shape_palm_pcd_pose.pose.orientation.w]
        palm_pcd_pose_key = object_grasp_id + '_close_shape_palm_pcd_pose'
        if palm_pcd_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_pcd_pose_key, 
                    data=close_shape_palm_pcd_pose_list)

        close_shape_palm_world_pose_list = [req.close_shape_palm_world_pose.pose.position.x, req.close_shape_palm_world_pose.pose.position.y,
               req.close_shape_palm_world_pose.pose.position.z, req.close_shape_palm_world_pose.pose.orientation.x, 
               req.close_shape_palm_world_pose.pose.orientation.y, req.close_shape_palm_world_pose.pose.orientation.z, 
               req.close_shape_palm_world_pose.pose.orientation.w]
        palm_world_pose_key = object_grasp_id + '_close_shape_palm_world_pose'
        if palm_world_pose_key not in grasp_file:
            grasp_file.create_dataset(palm_world_pose_key, 
                    data=close_shape_palm_world_pose_list)

        object_world_pose_list = [req.object_world_pose.pose.position.x, req.object_world_pose.pose.position.y,
               req.object_world_pose.pose.position.z, req.object_world_pose.pose.orientation.x,
               req.object_world_pose.pose.orientation.y, req.object_world_pose.pose.orientation.z, 
               req.object_world_pose.pose.orientation.w]
        object_world_pose_key = object_grasp_id + '_object_world_pose'
        if object_world_pose_key not in grasp_file:
            grasp_file.create_dataset(object_world_pose_key, 
                    data=object_world_pose_list)

        preshape_js_position_key = object_grasp_id + '_preshape_joint_state_position'
        if preshape_js_position_key not in grasp_file:
            grasp_file.create_dataset(preshape_js_position_key, 
                    data=req.preshape_allegro_joint_state.position)

        true_preshape_js_position_key = object_grasp_id + '_true_preshape_js_position'
        if true_preshape_js_position_key not in grasp_file:
            grasp_file.create_dataset(true_preshape_js_position_key, 
                    data=req.true_preshape_joint_state.position)

        close_js_position_key = object_grasp_id + '_close_shape_joint_state_position'
        if close_js_position_key not in grasp_file:
            grasp_file.create_dataset(close_js_position_key, 
                    data=req.close_shape_allegro_joint_state.position)

        grasp_label_key = object_grasp_id + '_grasp_label' 
        if grasp_label_key not in grasp_file:
            grasp_file.create_dataset(grasp_label_key, 
                    data=req.grasp_success_label)

            suc_prob_key = object_grasp_id + '_suc_prob' 
            if suc_prob_key not in grasp_file:
                grasp_file.create_dataset(suc_prob_key, data=req.suc_prob)

            grasp_file['total_grasps_num'][()] += 1
            if req.grasp_id % self.grasp_inits_inf_num == 0:
                #Inference grasp
                grasp_file['total_inf_grasps_num'][()] += 1
                grasp_file[object_inf_grasps_num_key][()] += 1

                grasp_file['total_inf_suc_prob'][()] += req.suc_prob
                grasp_file['avg_inf_suc_prob'][()] = grasp_file['total_inf_suc_prob'][()] \
                                                    / grasp_file['total_inf_grasps_num'][()]

                grasp_file[object_total_inf_suc_prob_key][()] += req.suc_prob
                grasp_file[object_avg_inf_suc_prob_key][()] = grasp_file[object_total_inf_suc_prob_key][()] \
                                                            / grasp_file[object_inf_grasps_num_key][()]

                max_suc_prob_init_idx_key = object_grasp_id + '_max_suc_prob_init_idx' 
                if max_suc_prob_init_idx_key not in grasp_file:
                    grasp_file.create_dataset(max_suc_prob_init_idx_key, data=req.max_inf_suc_prob_init_idx)
            else:
                #Initializations grasps
                grasp_file['total_init_grasps_num'][()] += 1
                grasp_file[object_init_grasps_num_key][()] += 1

                grasp_file['total_init_suc_prob'][()] += req.suc_prob
                grasp_file['avg_init_suc_prob'][()] = grasp_file['total_init_suc_prob'][()] \
                                                    / grasp_file['total_init_grasps_num'][()]

                grasp_file[object_total_init_suc_prob_key][()] += req.suc_prob
                grasp_file[object_avg_init_suc_prob_key][()] = grasp_file[object_total_init_suc_prob_key][()] \
                                                            / grasp_file[object_init_grasps_num_key][()]

            if req.grasp_success_label == 1:
                grasp_file['suc_grasps_num'][()] += 1
                if req.grasp_id % self.grasp_inits_inf_num == 0:
                    #Inference grasp
                    grasp_file['suc_inf_grasps_num'][()] += 1
                    grasp_file[object_suc_inf_grasps_num_key][()] += 1

                    grasp_file[object_total_suc_inf_suc_prob_key][()] += req.suc_prob
                    grasp_file[object_avg_suc_inf_suc_prob_key][()] = grasp_file[object_total_suc_inf_suc_prob_key][()] \
                                                        / grasp_file['suc_inf_grasps_num'][()]
                else:
                    #Initializations grasps
                    grasp_file['suc_init_grasps_num'][()] += 1
                    grasp_file[object_suc_init_grasps_num_key][()] += 1
            else:
                if req.grasp_id % self.grasp_inits_inf_num == 0:
                    grasp_file[object_total_fail_inf_suc_prob_key][()] += req.suc_prob
                    failing_inf_num = grasp_file['total_inf_grasps_num'][()] - grasp_file['suc_inf_grasps_num'][()]
                    grasp_file[object_avg_fail_inf_suc_prob_key][()] = grasp_file[object_total_fail_inf_suc_prob_key][()] \
                                                        / failing_inf_num

        top_grasp_key = object_grasp_id + '_top_grasp'
        if top_grasp_key not in grasp_file:
            grasp_file.create_dataset(top_grasp_key,
                    data=req.top_grasp)

        # Compute the finger tip and palm center locations in image space and record.
        # Preshape
        self.compute_finger_tip_loc.set_up_input(req.true_preshape_palm_pcd_pose, req.true_preshape_joint_state)

        #if self.use_hd:
        #    self.compute_finger_tip_loc.set_up_input(req.preshape_palm_pcd_pose, req.preshape_allegro_joint_state)
        #else:
        #    self.compute_finger_tip_loc.set_up_input(req.preshape_palm_sd_pcd_pose, req.preshape_allegro_joint_state)
        self.compute_finger_tip_loc.proj_finger_palm_locs_to_img()
        preshape_palm_image_loc = self.compute_finger_tip_loc.palm_image_loc
        preshape_finger_tip_image_locs = self.compute_finger_tip_loc.finger_tip_image_locs 
        #preshape_finger_tip_pcd_poses = self.compute_finger_tip_loc.finger_tip_poses 
        # Closed hand shape
        #self.compute_finger_tip_loc.set_up_input(req.preshape_palm_pcd_pose, req.close_shape_allegro_joint_state,
        #                                            self.camera_info)
        #self.compute_finger_tip_loc.proj_finger_palm_locs_to_img()
        #close_shape_finger_tip_image_locs = self.compute_finger_tip_loc.finger_tip_image_locs 

        preshape_palm_img_loc_key = object_grasp_id + '_preshape_palm_img_loc'
        if preshape_palm_img_loc_key not in grasp_file:
            grasp_file.create_dataset(preshape_palm_img_loc_key, data=preshape_palm_image_loc)

        preshape_finger_tip_img_locs_key = object_grasp_id + '_preshape_finger_tip_img_locs'
        if preshape_finger_tip_img_locs_key not in grasp_file:
            grasp_file.create_dataset(preshape_finger_tip_img_locs_key, data=preshape_finger_tip_image_locs)

        #preshape_finger_tip_pcd_poses_key = object_grasp_id + '_preshape_finger_tip_pcd_poses'
        #if preshape_finger_tip_pcd_poses_key not in grasp_file:
        #    print 'preshape_finger_tip_pcd_poses:', preshape_finger_tip_pcd_poses
        #    grasp_file.create_dataset(preshape_finger_tip_pcd_poses_key, data=preshape_finger_tip_pcd_poses)
        #
        #close_shape_finger_tip_img_locs_key = object_grasp_id + '_close_shape_finger_tip_img_locs'
        #if close_shape_finger_tip_img_locs_key not in grasp_file:
        #    grasp_file.create_dataset(close_shape_finger_tip_img_locs_key, close_shape_finger_tip_image_locs)

        response = GraspDataRecordingResponse()
        response.object_id = object_id
        response.preshape_palm_image_loc = preshape_palm_image_loc.tolist()
        response.preshape_finger_tip_image_locs = preshape_finger_tip_image_locs.ravel().tolist()
        #response.close_shape_finger_tip_image_locs = close_shape_finger_tip_image_locs.ravel().tolist()
        response.save_h5_success = True
        grasp_file.close()
        return response

    def create_record_data_server(self):
        rospy.Service('record_grasp_data', GraspDataRecording, self.handle_record_grasp_data)
        rospy.loginfo('Service record_grasp_data:')
        rospy.loginfo('Ready to record grasp data.')

if __name__ == '__main__':
    record_grasp_data = RecordGraspData()
    record_grasp_data.create_record_data_server()
    rospy.spin()

