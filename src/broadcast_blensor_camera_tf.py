#!/usr/bin/env python
import rospy
import tf
import numpy as np

def broadcast_blensor_cam_tf():
    tf_br = tf.TransformBroadcaster()
    #palm_quaternion = tf.transformations.quaternion_from_euler(np.deg2rad(65), np.deg2rad(-15), np.deg2rad(-90))
    #tf_br.sendTransform((-1.2, -0.58, 1.2),
    #        tuple(palm_quaternion),
    #        rospy.Time.now(), 'blensor_camera', 'world')
    palm_quaternion = tf.transformations.quaternion_from_euler(np.deg2rad(50), np.deg2rad(0), np.deg2rad(-90))
    tf_br.sendTransform((-1.2, -0.75, 1.3),
            tuple(palm_quaternion),
            rospy.Time.now(), 'blensor_camera', 'world')



if __name__ == '__main__':
    rospy.init_node('blensor_camera_tf_br')
    #broadcast_blensor_cam_tf()
    #rospy.spin()
    while not rospy.is_shutdown():
        broadcast_blensor_cam_tf()
        rospy.sleep(1)


