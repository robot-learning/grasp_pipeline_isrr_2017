#!/usr/bin/env python

import roslib; roslib.load_manifest('grasp_data_collection_pkg')
import rospy
from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
#from grasp_data_collection_pkg import srv
from grasp_data_collection_pkg.srv import *
from tabletop_obj_segmentation.srv import *
from grasps_detection_cnn_gd_pkg.srv import *
import os
import tf
from sensor_msgs.msg import JointState, Image
from grasp_demo.srv import *
import copy
import numpy as np
import time
from blensor_ros.srv import *
import cv2
import time
from gazebo_msgs.msg import ModelStates
import h5py
from cv_bridge import CvBridge, CvBridgeError

class GraspDataCollectionClient:
    def __init__(self):
        rospy.init_node('grasp_data_collection_client')
        self.use_sim = rospy.get_param('~use_sim', False)
        self.use_hd = rospy.get_param('~use_hd', True)
        #self.num_grasps_per_object = rospy.get_param('~num_grasps_per_object', 10)
        self.num_poses_per_object = rospy.get_param('~num_poses_per_object', 10)
        self.save_visual_data_pre_path = rospy.get_param('~save_visual_data_pre_path', '')
        self.mount_desired_world = None
        self.set_up_place_range()
        self.mount_desired_world = None
        self.palm_desired_world = None
        self.object_world_pose = None
        self.kinect2_sd_frame_id = 'kinect2_ir_optical_frame'
        self.listener = tf.TransformListener()
        #self.bb_faces_num = 3
        self.table_len_z = 0.4
        self.min_palm_dist_to_table = rospy.get_param('~min_palm_dist_to_table', 0.04)
        self.min_palm_height = self.table_len_z + self.min_palm_dist_to_table
        self.max_palm_dist_to_table = rospy.get_param('~max_palm_dist_to_table', 0.06)
        self.max_palm_height = self.table_len_z + self.max_palm_dist_to_table
        self.lift_height = 0.15
        self.lift_times = 4#2
        lift_dist_suc_range = 0.05
        self.grasp_success_object_height = self.table_len_z + self.lift_height - lift_dist_suc_range

        self.data_recording_path = rospy.get_param('~data_recording_path', '/media/kai/multi_finger_grasp_data/')
        self.grasp_file_name = self.data_recording_path + 'grasp_data.h5'

        gazebo_model_state_topic='/gazebo/model_states'
        rospy.Subscriber(gazebo_model_state_topic, ModelStates, self.get_object_pose_from_gazebo)

        hand_joint_states_topic = '/allegro_hand_right/joint_states'
        rospy.Subscriber(hand_joint_states_topic, JointState, self.get_hand_joint_state)
        gazebo_camera_rgb_topic = '/kinect/camera/rgb/image_raw'
        rospy.Subscriber(gazebo_camera_rgb_topic, Image, self.get_gazebo_rgb_image)
        self.bridge = CvBridge()
        self.save_grasp_snap = rospy.get_param('~save_grasp_snap', False)

        self.joint_vel_thresh = .1
        self.sampling_grasp = False

    def set_up_object_name(self, object_name, object_mesh_path=None):
        self.object_name = object_name
        #self.grasp_id = grasp_id
        if object_mesh_path is not None:
            self.object_mesh_path = object_mesh_path

    def set_up_grasp_id(self, grasp_id):
        self.grasp_id = grasp_id

    def set_up_object_pose(self, object_pose):
        self.object_world_pose = object_pose

    def get_last_object_id_name(self):
        grasp_file = h5py.File(self.grasp_file_name, 'r')
        last_object_id = grasp_file['max_object_id'][()]
        self.last_object_name = grasp_file['cur_object_name'][()]
        self.cur_object_id = last_object_id + 1
        grasp_file.close()

    def get_hand_joint_state(self, hand_js):
        self.true_hand_joint_state = hand_js 

    def get_gazebo_rgb_image(self, rgb_image_msg):
        self.gazebo_rgb_image_msg = None
        try:
            self.gazebo_rgb_image_msg = rgb_image_msg
            #self.gazebo_rgb_image = self.bridge.imgmsg_to_cv2(rgb_image_msg, "bgr8")
        except CvBridgeError as e:
            rospy.logerr(e)

    def set_up_place_range(self):
        #Box table position: x: 0 y: -0.75 z: 0.2
        #Box dimension: 1, 1, 0.4
        #self.table_x = 0
        #self.table_y = -0.75
        #self.table_z = 0.2
        #self.table_len_x = 1.
        #self.table_len_y = 1.
        #self.table_len_z = 0.4
        #x_edge_not_to_place = 0.4
        #y_edge_not_to_place = 0.4
        #place_x_half_range = (self.table_len_x - x_edge_not_to_place) * .5                
        #place_y_half_range = (self.table_len_y - y_edge_not_to_place) * .5
        #self.place_x_min = self.table_x - place_x_half_range
        #self.place_x_max = self.table_x + place_x_half_range  
        #self.place_y_min = self.table_y - place_y_half_range
        #self.place_y_max = self.table_y + place_y_half_range  

        self.table_x = 0.
        self.table_y = -0.5
        self.table_z = 0.2
        self.table_len_z = 0.4
        place_x_half_range = 0.1 
        place_y_half_range = 0.1
        self.place_x_min = self.table_x - place_x_half_range
        self.place_x_max = self.table_x + place_x_half_range  
        self.place_y_min = self.table_y - place_y_half_range
        self.place_y_max = self.table_y + place_y_half_range  

    def create_moveit_scene_client(self, object_pose):
        rospy.loginfo('Waiting for service create_moveit_scene.')
        rospy.wait_for_service('create_moveit_scene')
        rospy.loginfo('Calling service create_moveit_scene.')
        try:
            create_scene_proxy = rospy.ServiceProxy('create_moveit_scene', ManageMoveitScene)
            create_scene_request = ManageMoveitSceneRequest()
            create_scene_request.create_scene = True
            create_scene_request.object_mesh_path = self.object_mesh_path
            create_scene_request.object_pose = object_pose 
            self.create_scene_response = create_scene_proxy(create_scene_request) 
            #print self.create_scene_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service create_moveit_scene call failed: %s'%e)
        rospy.loginfo('Service create_moveit_scene is executed %s.' %str(self.create_scene_response))

    def segment_object_client(self):
        rospy.loginfo('Waiting for service object_segmenter.')
        rospy.wait_for_service('object_segmenter')
        rospy.loginfo('Calling service object_segmenter.')
        try:
            object_segment_proxy = rospy.ServiceProxy('object_segmenter', SegmentGraspObject)
            object_segment_request = SegmentGraspObjectRequest()
            self.object_segment_response = object_segment_proxy(object_segment_request) 
            #print self.object_segment_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service object_segmenter call failed: %s'%e)
        rospy.loginfo('Service object_segmenter is executed.')

    def segment_object_blensor_client(self):
        rospy.loginfo('Waiting for service object_segmenter_blensor.')
        rospy.wait_for_service('object_segmenter_blensor')
        rospy.loginfo('Calling service object_segmenter_blensor.')
        try:
            object_segment_blensor_proxy = rospy.ServiceProxy('object_segmenter_blensor', SegmentGraspObjectBlensor)
            object_segment_blensor_request = SegmentGraspObjectBlensorRequest()
            object_segment_blensor_request.scene_cloud = self.get_blensor_data_response.cloud 
            object_segment_blensor_request.cloud_normal_save_path = self.save_visual_data_pre_path + \
                'cloud_normal/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                '_grasp_' + str(self.grasp_id) + '.pcd' 
             
            self.cloud_normal_save_path = object_segment_blensor_request.cloud_normal_save_path

            self.object_segment_blensor_response = object_segment_blensor_proxy(object_segment_blensor_request) 
            #print self.object_segment_blensor_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service object_segmenter_blensor call failed: %s'%e)
        rospy.loginfo('Service object_segmenter_blensor is executed.')
        if not self.object_segment_blensor_response.object_found:
            rospy.logerr('No object found from segmentation!')
            return False
        return True

    def gen_grasp_preshape_client(self):
        rospy.loginfo('Waiting for service gen_grasp_preshape.')
        rospy.wait_for_service('gen_grasp_preshape')
        rospy.loginfo('Calling service gen_grasp_preshape.')
        try:
            preshape_proxy = rospy.ServiceProxy('gen_grasp_preshape', GraspPreshape)
            preshape_request = GraspPreshapeRequest()
            if not self.use_sim:
                preshape_request.obj = self.object_segment_response.obj
            else:
                preshape_request.obj = self.object_segment_blensor_response.obj
                preshape_request.sample = self.sampling_grasp

            # Testing in simulation
            #preshape_req.obj = self.object_segment_response.obj
            self.preshape_response = preshape_proxy(preshape_request) 
            #self.preshape_palm_goal_pose_sd_pcd = self.listener.transformPose(self.kinect2_sd_frame_id, 
            #                                    self.preshape_response.palm_goal_pose_in_pcd)
            #print self.preshape_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service gen_grasp_preshape call failed: %s'%e)
        rospy.loginfo('Service gen_grasp_preshape is executed.')

    def update_detection_grasp_preshape_client(self, update_preshape):
        rospy.loginfo('Waiting for service update_detection_grasp_preshape.')
        rospy.wait_for_service('update_detection_grasp_preshape')
        rospy.loginfo('Calling service update_detection_grasp_preshape.')
        try:
            update_preshape_proxy = rospy.ServiceProxy('update_detection_grasp_preshape', 
                                                UpdateDetectedGraspPreshape)
            update_preshape_request = UpdateDetectedGraspPreshapeRequest()
            update_preshape_request.palm_goal_pose_in_pcd_det = update_preshape 
            update_preshape_response = update_preshape_proxy(update_preshape_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_detection_grasp_preshape call failed: %s'%e)
        rospy.loginfo('Service update_detection_grasp_preshape is executed.')

   
    def control_allegro_config_client(self, go_home=False, close_hand=False, grasp_preshape_idx=-1):
        rospy.loginfo('Waiting for service control_allegro_config.')
        rospy.wait_for_service('control_allegro_config')
        rospy.loginfo('Calling service control_allegro_config.')
        try:
            control_proxy = rospy.ServiceProxy('control_allegro_config', AllegroConfig)
            control_request = AllegroConfigRequest()
            if go_home:
                control_request.go_home = True
            elif close_hand:
                control_request.close_hand = True
            else:
                control_request.allegro_target_joint_state = self.preshape_response.allegro_joint_state[grasp_preshape_idx] 
                #rospy.loginfo('###############')
                #rospy.loginfo(control_request.allegro_target_joint_state)
            self.control_response = control_proxy(control_request) 
            #print self.control_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service control_allegro_config call failed: %s'%e)
        rospy.loginfo('Service control_allegro_config is executed %s.'%str(self.control_response))

    def listen_palm_pose(self, grasp_preshape_idx):
        self.mount_desired_world = None
        self.palm_desired_world = None
        #if grasp_preshape_idx == len(self.preshape_response.allegro_joint_state) - 1:
        #    self.object_world_pose = None
        #self.object_world_pose = None
        self.true_palm_pose_pcd = None
        self.true_palm_pose_world = None

        #if grasp_preshape_idx == 0:
        #    palm_goal_tf_name = 'palm_goal_' + str(self.max_inf_suc_prob_init_idx)
        #else:
        #    palm_goal_tf_name = 'palm_goal_' + str(grasp_preshape_idx - 1)

        palm_goal_tf_name = 'palm_goal_' + str(grasp_preshape_idx)

        rate = rospy.Rate(10.0)
        i = 0
        while (not rospy.is_shutdown()) and i < 10:
            try:
                if self.mount_desired_world is None:
                    (trans_m_to_p, rot_m_to_p) = self.listener.lookupTransform('palm_link', 'allegro_mount', rospy.Time(0))
                    mount_desired_pose = PoseStamped()
                    mount_desired_pose.header.frame_id = palm_goal_tf_name 
                    mount_desired_pose.pose.position.x = trans_m_to_p[0]
                    mount_desired_pose.pose.position.y = trans_m_to_p[1]
                    mount_desired_pose.pose.position.z = trans_m_to_p[2]
                    mount_desired_pose.pose.orientation.x = rot_m_to_p[0]
                    mount_desired_pose.pose.orientation.y = rot_m_to_p[1]
                    mount_desired_pose.pose.orientation.z = rot_m_to_p[2]
                    mount_desired_pose.pose.orientation.w = rot_m_to_p[3]
                
                    self.mount_desired_world = self.listener.transformPose('world', mount_desired_pose)

                if self.palm_desired_world is None:
                    (trans_p_to_w, rot_p_to_w) = self.listener.lookupTransform('world', palm_goal_tf_name, rospy.Time(0))
                    palm_desired_world = PoseStamped()
                    palm_desired_world.header.frame_id = 'world'
                    palm_desired_world.pose.position.x = trans_p_to_w[0]
                    palm_desired_world.pose.position.y = trans_p_to_w[1]
                    palm_desired_world.pose.position.z = trans_p_to_w[2]
                    palm_desired_world.pose.orientation.x = rot_p_to_w[0]
                    palm_desired_world.pose.orientation.y = rot_p_to_w[1]
                    palm_desired_world.pose.orientation.z = rot_p_to_w[2]
                    palm_desired_world.pose.orientation.w = rot_p_to_w[3]
    
                    self.palm_desired_world = palm_desired_world

                #if self.object_world_pose is None:
                #    (trans_o_to_w, rot_o_to_w) = self.listener.lookupTransform('world', 'grasp_object', rospy.Time(0))
                #    object_world_pose = PoseStamped()
                #    object_world_pose.header.frame_id = 'world'
                #    object_world_pose.pose.position.x = trans_o_to_w[0]
                #    object_world_pose.pose.position.y = trans_o_to_w[1]
                #    object_world_pose.pose.position.z = trans_o_to_w[2]
                #    object_world_pose.pose.orientation.x = rot_o_to_w[0]
                #    object_world_pose.pose.orientation.y = rot_o_to_w[1]
                #    object_world_pose.pose.orientation.z = rot_o_to_w[2]
                #    object_world_pose.pose.orientation.w = rot_o_to_w[3]

                #    self.object_world_pose = object_world_pose
          
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            i += 1
            rate.sleep()

    def listen_true_palm_pose(self):
        '''
        Listen to get the true palm pose in camera and world frame. 
        This is necesssary because 
        '''
        #self.true_palm_pose_pcd = None
        #self.true_palm_pose_world = None
        
        true_palm_pose_pcd = None
        true_palm_pose_world = None

        rate = rospy.Rate(10.0)
        i = 0
        while (not rospy.is_shutdown()) and i < 10:
            try:
                if true_palm_pose_world is None:
                    (trans_p_to_w, rot_p_to_w) = self.listener.lookupTransform('world', 'palm_link', rospy.Time(0))
                    true_palm_pose_world = PoseStamped()
                    true_palm_pose_world.header.frame_id = 'world'
                    true_palm_pose_world.pose.position.x = trans_p_to_w[0]
                    true_palm_pose_world.pose.position.y = trans_p_to_w[1]
                    true_palm_pose_world.pose.position.z = trans_p_to_w[2]
                    true_palm_pose_world.pose.orientation.x = rot_p_to_w[0]
                    true_palm_pose_world.pose.orientation.y = rot_p_to_w[1]
                    true_palm_pose_world.pose.orientation.z = rot_p_to_w[2]
                    true_palm_pose_world.pose.orientation.w = rot_p_to_w[3]
    
                    #self.true_palm_pose_world = true_palm_pose_world

                if true_palm_pose_pcd is None:
                    (trans_p_to_w, rot_p_to_w) = self.listener.lookupTransform('blensor_camera', 'palm_link', rospy.Time(0))
                    true_palm_pose_pcd = PoseStamped()
                    true_palm_pose_pcd.header.frame_id = 'blensor_camera'
                    true_palm_pose_pcd.pose.position.x = trans_p_to_w[0]
                    true_palm_pose_pcd.pose.position.y = trans_p_to_w[1]
                    true_palm_pose_pcd.pose.position.z = trans_p_to_w[2]
                    true_palm_pose_pcd.pose.orientation.x = rot_p_to_w[0]
                    true_palm_pose_pcd.pose.orientation.y = rot_p_to_w[1]
                    true_palm_pose_pcd.pose.orientation.z = rot_p_to_w[2]
                    true_palm_pose_pcd.pose.orientation.w = rot_p_to_w[3]
    
                    #self.true_palm_pose_pcd = true_palm_pose_pcd

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                continue
            i += 1
            rate.sleep()

        return true_palm_pose_pcd, true_palm_pose_world 

    def arm_moveit_planner_client(self, go_home=False, place_goal_pose=None):
        rospy.loginfo('Waiting for service moveit_cartesian_pose_planner.')
        rospy.wait_for_service('moveit_cartesian_pose_planner')
        rospy.loginfo('Calling service moveit_cartesian_pose_planner.')
        try:
            planning_proxy = rospy.ServiceProxy('moveit_cartesian_pose_planner', PalmGoalPoseWorld)
            planning_request = PalmGoalPoseWorldRequest()
            if go_home:
                planning_request.go_home = True
            elif place_goal_pose is not None:
                planning_request.palm_goal_pose_world = place_goal_pose
            else:
                planning_request.palm_goal_pose_world = self.mount_desired_world.pose
            self.planning_response = planning_proxy(planning_request) 
            #print self.planning_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service moveit_cartesian_pose_planner call failed: %s'%e)
        rospy.loginfo('Service moveit_cartesian_pose_planner is executed %s.'%str(self.planning_response))
        return self.planning_response.success

    def arm_movement_client(self):
        rospy.loginfo('Waiting for service arm_movement.')
        rospy.wait_for_service('arm_movement')
        rospy.loginfo('Calling service arm_movement.')
        try:
            movement_proxy = rospy.ServiceProxy('arm_movement', MoveArm)
            movement_request = MoveArmRequest()
            self.movement_response = movement_proxy(movement_request) 
            #print self.movement_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service arm_movement call failed: %s'%e)
        rospy.loginfo('Service arm_movement is executed %s.'%str(self.movement_response))

    def grasp_client(self, top_grasp):
        rospy.loginfo('Waiting for service grasp.')
        rospy.wait_for_service('grasp')
        rospy.loginfo('Calling service grasp.')
        try:
            grasp_proxy = rospy.ServiceProxy('grasp', GraspAllegro)
            grasp_request = GraspAllegroRequest()
            grasp_request.joint_vel_thresh = self.joint_vel_thresh 
            grasp_request.top_grasp = top_grasp
            self.grasp_response = grasp_proxy(grasp_request) 
            #print self.grasp_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service grasp call failed: %s'%e)
        rospy.loginfo('Service grasp is executed %s.'%self.grasp_response.success)

    def clean_moveit_scene_client(self):
        rospy.loginfo('Waiting for service clean_moveit_scene.')
        rospy.wait_for_service('clean_moveit_scene')
        rospy.loginfo('Calling service clean_moveit_scene.')
        try:
            clean_scene_proxy = rospy.ServiceProxy('clean_moveit_scene', ManageMoveitScene)
            clean_scene_request = ManageMoveitSceneRequest()
            clean_scene_request.clean_scene = True
            self.clean_scene_response = clean_scene_proxy(clean_scene_request) 
            #print self.clean_scene_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service clean_moveit_scene call failed: %s'%e)
        rospy.loginfo('Service clean_moveit_scene is executed %s.' %str(self.clean_scene_response))

    def lift_moveit_planner_client(self, height_to_lift):
        rospy.loginfo('Waiting for service moveit_cartesian_pose_planner to lift.')
        rospy.wait_for_service('moveit_cartesian_pose_planner')
        rospy.loginfo('Calling service moveit_cartesian_pose_planner to lift.')
        try:
            planning_proxy = rospy.ServiceProxy('moveit_cartesian_pose_planner', PalmGoalPoseWorld)
            planning_request = PalmGoalPoseWorldRequest()
            planning_request.palm_goal_pose_world = copy.deepcopy(self.mount_desired_world.pose)
            #planning_request.palm_goal_pose_world.position.z += self.lift_height
            planning_request.palm_goal_pose_world.position.z += height_to_lift
            ##print 'palm_pose for liftting:', planning_request.palm_goal_pose_world
            planning_request.lift_way_points = False
            #planning_request.lift_height = self.lift_height
            self.planning_response = planning_proxy(planning_request) 
            #print self.planning_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service moveit_cartesian_pose_planner call to lift failed: %s'%e)
        rospy.loginfo('Service moveit_cartesian_pose_planner to lift is executed %s.'%str(self.planning_response))

    def get_object_pose_from_gazebo(self, gz_model_msg):
        #rostopic echo /gazebo/model_states
        self.object_name_gazebo = gz_model_msg.name[-1]
        self.object_pose_gazebo = gz_model_msg.pose[-1]

    def get_grasp_label(self):
        if self.object_name_gazebo != self.object_name:
            rospy.logerr('Got wrong objects from Gazebo!')
        grasp_success = 0
        if self.object_pose_gazebo.position.z >= self.grasp_success_object_height:
            grasp_success = 1
        return grasp_success

    def lift_arm_movement_client(self):
        rospy.loginfo('Waiting for service arm_movement to lift.')
        rospy.wait_for_service('arm_movement')
        rospy.loginfo('Calling service arm_movement to lift.')
        try:
            movement_proxy = rospy.ServiceProxy('arm_movement', MoveArm)
            movement_request = MoveArmRequest()
            self.movement_response = movement_proxy(movement_request) 
            #print self.movement_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service arm_movement call to lift failed: %s'%e)
        rospy.loginfo('Service arm_movement to lift is executed %s.'%str(self.movement_response))

    def place_arm_movement_client(self):
        rospy.loginfo('Move the arm to palce the object back.')
        place_x_loc = np.random.uniform(self.place_x_min, self.place_x_max) 
        place_y_loc = np.random.uniform(self.place_y_min, self.place_y_max) 
        place_pose = copy.deepcopy(self.mount_desired_world.pose)
        place_pose.position.x = place_x_loc
        place_pose.position.y = place_y_loc
        #print 'place_pose:', place_pose
        dc_client.arm_moveit_planner_client(place_goal_pose=place_pose)
        dc_client.arm_movement_client()

    def place_control_allegro_client(self):
        rospy.loginfo('Open the allegro hand to place the object.')
        self.control_allegro_config_client(go_home=True)

    def move_arm_home_client(self):
        rospy.loginfo('Move the arm to go home.')
        dc_client.arm_moveit_planner_client(go_home=True)
        dc_client.arm_movement_client()

    def record_grasp_visual_data_client(self, record_grasp_data_request):
        rospy.loginfo('Waiting for service save_visual_data.')
        rospy.wait_for_service('save_visual_data')
        rospy.loginfo('Calling service save_visual_data.')
        try:
            save_visual_data_proxy = rospy.ServiceProxy('save_visual_data', GraspDataRecording)
            self.save_visual_data_response = save_visual_data_proxy(record_grasp_data_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service save_visual_data call failed: %s'%e)
        rospy.loginfo('Service save_visual_data is executed %s.'%self.save_visual_data_response.save_visual_data_success)

    def record_grasp_data_client(self, grasp_preshape_idx):
        keep_current_grasp = True
        rospy.loginfo('Waiting for service record_grasp_data.')
        rospy.wait_for_service('record_grasp_data')
        rospy.loginfo('Calling service record_grasp_data.')
        try:
            record_grasp_data_proxy = rospy.ServiceProxy('record_grasp_data', GraspDataRecording)
            record_grasp_data_request = GraspDataRecordingRequest()
            record_grasp_data_request.object_name = self.object_name
            record_grasp_data_request.grasp_id = self.grasp_id
            record_grasp_data_request.time_stamp = time.time()

            record_grasp_data_request.sample = self.sampling_grasp

            grasp_label = self.get_grasp_label()
            record_grasp_data_request.grasp_success_label = grasp_label 

            # Record the grasp image from gazebo
            if self.save_grasp_snap and self.gazebo_rgb_image_msg is not None:
                self.gazebo_rgb_image = self.bridge.imgmsg_to_cv2(self.gazebo_rgb_image_msg, "bgr8")
                if grasp_label:
                    path_to_save_gazebo_rgb = self.save_visual_data_pre_path + \
                        'gazebo_rgb_image/suc_grasps/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                        '_grasp_' + str(self.grasp_id) + '.ppm' 
                else:
                    path_to_save_gazebo_rgb = self.save_visual_data_pre_path + \
                        'gazebo_rgb_image/fail_grasps/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                        '_grasp_' + str(self.grasp_id) + '.ppm' 

                cv2.imwrite(path_to_save_gazebo_rgb, self.gazebo_rgb_image)


            record_grasp_data_request.object_world_pose = self.object_world_pose
            record_grasp_data_request.preshape_palm_world_pose = self.palm_desired_world
            record_grasp_data_request.preshape_palm_pcd_pose = self.preshape_response.palm_goal_pose_in_pcd[grasp_preshape_idx]
            #record_grasp_data_request.preshape_palm_sd_pcd_pose = self.preshape_palm_goal_pose_sd_pcd
            record_grasp_data_request.preshape_allegro_joint_state = self.preshape_response.allegro_joint_state[grasp_preshape_idx] 
            record_grasp_data_request.true_preshape_joint_state = self.true_preshape_hand_js
            record_grasp_data_request.true_preshape_palm_world_pose = self.true_palm_pose_world
            record_grasp_data_request.true_preshape_palm_pcd_pose = self.true_palm_pose_pcd
            #record_grasp_data_request.close_shape_allegro_joint_state = self.grasp_response.final_joint_state 
            record_grasp_data_request.close_shape_allegro_joint_state = self.close_hand_js  
            record_grasp_data_request.close_shape_palm_pcd_pose = self.close_palm_pose_pcd 
            record_grasp_data_request.close_shape_palm_world_pose = self.close_palm_pose_world 
            record_grasp_data_request.top_grasp = self.preshape_response.is_top_grasp[grasp_preshape_idx] 

            record_grasp_data_request.suc_prob = self.inits_inf_suc_prob[grasp_preshape_idx]
            record_grasp_data_request.max_inf_suc_prob_init_idx = self.max_inf_suc_prob_init_idx

            self.record_grasp_data_response = record_grasp_data_proxy(record_grasp_data_request) 
            rospy.loginfo('****' + str(self.record_grasp_data_response))
        except rospy.ServiceException, e:
            rospy.loginfo('Service record_grasp_data call failed: %s'%e)
        rospy.loginfo('Service record_grasp_data is executed %s.'%self.record_grasp_data_response.save_h5_success)

        self.record_grasp_data_request = record_grasp_data_request
        #rospy.loginfo('****##' + str(self.record_grasp_data_response))
        return keep_current_grasp

    def grasp_inference_client(self, grasp_preshape_idx):
        rospy.loginfo('Waiting for service grasps_inference.')
        rospy.wait_for_service('grasps_inference')
        rospy.loginfo('Calling service grasps_inference.')
        try:
            grasp_inf_proxy = rospy.ServiceProxy('grasps_inference', GraspInference)
            grasp_inf_request = GraspInferenceRequest()
            #grasp_inf_request.rgbd_info.scene_cloud_normal = self.cloud_normal_save_path
            grasp_inf_request.rgbd_info.scene_cloud_normal = self.object_segment_blensor_response.scene_cloud_normal
            grasp_inf_request.rgb_image_path = self.path_to_save_rgb 
            grasp_inf_request.depth_image_path = self.path_to_save_depth 

            #grasp_inf_request.init_hand_pcd_true_config.hand_joint_state = self.true_preshape_hand_js 
            #grasp_inf_request.init_hand_pcd_true_config.palm_pose = self.true_palm_pose_pcd

            grasp_inf_request.init_hand_pcd_goal_config.hand_joint_state = self.preshape_response.allegro_joint_state[grasp_preshape_idx] 
            grasp_inf_request.init_hand_pcd_goal_config.palm_pose = self.preshape_response.palm_goal_pose_in_pcd[grasp_preshape_idx]

            #grasp_inf_request.close_hand_pcd_config.hand_joint_state = self.close_hand_js 
            #grasp_inf_request.close_hand_pcd_config.palm_pose = self.close_palm_pose_pcd

            grasp_inf_request.object_name = self.object_name
            grasp_inf_request.grasp_id = self.grasp_id
            grasp_inf_request.object_id = self.cur_object_id
            #grasp_inf_request.grasp_success_label = self.record_grasp_data_request.grasp_success_label

            grasp_inf_request.is_top_grasp = self.preshape_response.is_top_grasp[grasp_preshape_idx] 

            grasp_inf_response = grasp_inf_proxy(grasp_inf_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service grasps_inference failed: %s'%e)
        rospy.loginfo('Service grasps_inference is executed %s.'%str(grasp_inf_response.success))

        return grasp_inf_response
        
    def grasp_sample_inference_client(self):
        rospy.loginfo('Waiting for service grasps_sample_inference.')
        rospy.wait_for_service('grasps_sample_inference')
        rospy.loginfo('Calling service grasps_sample_inference.')
        try:
            grasp_sample_inf_proxy = rospy.ServiceProxy('grasps_sample_inference', GraspSampleInf)
            grasp_sample_inf_request = GraspSampleInfRequest()
            #grasp_sample_inf_request.rgbd_info.scene_cloud_normal = self.cloud_normal_save_path
            grasp_sample_inf_request.rgbd_info.scene_cloud_normal = self.object_segment_blensor_response.scene_cloud_normal
            grasp_sample_inf_request.rgb_image_path = self.path_to_save_rgb 
            grasp_sample_inf_request.depth_image_path = self.path_to_save_depth 

            grasp_sample_inf_request.sample_hand_joint_state_list = self.preshape_response.allegro_joint_state 
            grasp_sample_inf_request.sample_palm_pose_list = self.preshape_response.palm_goal_pose_in_pcd

            grasp_sample_inf_request.object_name = self.object_name
            grasp_sample_inf_request.grasp_id = self.grasp_id
            grasp_sample_inf_request.object_id = self.cur_object_id

            grasp_sample_inf_request.sample_is_top_grasp_list = self.preshape_response.is_top_grasp 

            grasp_sample_inf_response = grasp_sample_inf_proxy(grasp_sample_inf_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service grasps_sample_inference failed: %s'%e)
        rospy.loginfo('Service grasps_sample_inference is executed %s.'%str(grasp_sample_inf_response.success))
 
        return grasp_sample_inf_response

    def test_moveit_scene_management(self, object_pose):
        #self.create_moveit_scene_client(object_pose)
        self.get_blensor_data_client()
        #self.clean_moveit_scene_client()
        return

    def segment_and_generate_preshape(self):
        self.get_blensor_data_client()
        object_found = self.segment_object_blensor_client()
        if not object_found:
            return False
        self.gen_grasp_preshape_client()
        
        if self.sampling_grasp:
            grasp_sample_inf_response = self.grasp_sample_inference_client()
            self.max_inf_suc_prob_init_idx = grasp_sample_inf_response.max_suc_prob_sample_idx
            self.inits_inf_suc_prob = [grasp_sample_inf_response.max_suc_prob] 
            self.preshape_response.palm_goal_pose_in_pcd = \
                    [self.preshape_response.palm_goal_pose_in_pcd[self.max_inf_suc_prob_init_idx]]
            self.preshape_response.allegro_joint_state = \
                    [self.preshape_response.allegro_joint_state[self.max_inf_suc_prob_init_idx]]
            self.preshape_response.is_top_grasp = \
                    [self.preshape_response.is_top_grasp[self.max_inf_suc_prob_init_idx]]
            self.update_detection_grasp_preshape_client(self.preshape_response.palm_goal_pose_in_pcd)

        else:
            grasp_inf_response_list = []
            max_suc_prob = -1.
            max_suc_prob_idx = -1
            inits_suc_prob_list = []
            for i in xrange(len(self.preshape_response.allegro_joint_state)):
                grasp_inf_response = self.grasp_inference_client(i) 
                print i
                print 'inf_suc_prob:', grasp_inf_response.inf_suc_prob
                if grasp_inf_response.inf_suc_prob >= max_suc_prob:
                    max_suc_prob = grasp_inf_response.inf_suc_prob
                    max_suc_prob_idx = i
                grasp_inf_response_list.append(grasp_inf_response)
                inits_suc_prob_list.append(grasp_inf_response.init_suc_prob)

            self.max_inf_suc_prob_init_idx = max_suc_prob_idx
            self.inits_inf_suc_prob = [max_suc_prob] + inits_suc_prob_list 
            self.preshape_response.palm_goal_pose_in_pcd = \
                    [grasp_inf_response_list[max_suc_prob_idx].inf_hand_pcd_config.palm_pose] + \
                    self.preshape_response.palm_goal_pose_in_pcd
            self.preshape_response.allegro_joint_state = \
                    [grasp_inf_response_list[max_suc_prob_idx].inf_hand_pcd_config.hand_joint_state] + \
                    self.preshape_response.allegro_joint_state
            self.preshape_response.is_top_grasp = \
                    [self.preshape_response.is_top_grasp[max_suc_prob_idx]] + \
                    self.preshape_response.is_top_grasp

            self.update_detection_grasp_preshape_client(self.preshape_response.palm_goal_pose_in_pcd)

        return True

    def grasp_and_lift_object_steps(self, object_pose, grasp_preshape_idx):
        #if self.use_sim:
        #    self.create_moveit_scene_client(object_pose)
        #    #rospy.sleep(5)
        #    self.get_blensor_data_client()
        #    self.segment_object_blensor_client()
        #else:
        #    self.segment_object_client()
        
        #if grasp_preshape_idx == 0:
        self.create_moveit_scene_client(object_pose)

        #self.gen_grasp_preshape_client()
        self.control_allegro_config_client(grasp_preshape_idx=grasp_preshape_idx)
        #rospy.loginfo(self.true_hand_joint_state)

        self.listen_palm_pose(grasp_preshape_idx)
        #rospy.loginfo('******************')
        #rospy.loginfo(self.mount_desired_world.pose.position.z)
        #rospy.loginfo(self.min_palm_height)

        #if self.mount_desired_world.pose.position.z < self.min_palm_height:
        #    rospy.loginfo('###Increase grasp height!')
        #    palm_rand_height = np.random.uniform(self.min_palm_height, self.max_palm_height)
        #    self.mount_desired_world.pose.position.z = palm_rand_height
        #    #self.mount_desired_world.pose.position.z = self.min_palm_height 

        moveit_found_plan = self.arm_moveit_planner_client()
        if not moveit_found_plan:
            return False
        self.arm_movement_client()

        self.true_palm_pose_pcd, self.true_palm_pose_world = self.listen_true_palm_pose()
        self.true_preshape_hand_js = self.true_hand_joint_state
        #rospy.loginfo(self.true_hand_joint_state)

        top_grasp = self.preshape_response.is_top_grasp[grasp_preshape_idx] 
        self.grasp_client(top_grasp)

        self.close_palm_pose_pcd, self.close_palm_pose_world = self.listen_true_palm_pose()
        self.close_hand_js = self.true_hand_joint_state 

        self.clean_moveit_scene_client()

        #self.lift_moveit_planner_client(self.lift_height / 2.)
        #self.lift_arm_movement_client() 
        #self.lift_moveit_planner_client(self.lift_height)
        #self.lift_arm_movement_client() 
        #Sleep for a while to make sure object doens't fall off.
        #rospy.sleep(1)
        for i in xrange(self.lift_times):
            self.lift_moveit_planner_client(self.lift_height * (i + 1) / self.lift_times)
            self.lift_arm_movement_client() 

        return True

    def place_object_steps(self, move_arm=True):
        #self.place_arm_movement_client()
        self.place_control_allegro_client()
        #if self.use_sim:
        #    self.create_moveit_scene_client()
        if move_arm:
            self.move_arm_home_client()

    def update_gazebo_object_client(self, object_name, object_pose_array, object_model_name):
        '''
            Gazebo management client to send request to create one new object and delete the 
            previous object.
        '''
        rospy.loginfo('Waiting for service update_gazebo_object.')
        rospy.wait_for_service('update_gazebo_object')
        rospy.loginfo('Calling service update_gazebo_object.')
        try:
            update_object_gazebo_proxy = rospy.ServiceProxy('update_gazebo_object', UpdateObjectGazebo)
            update_object_gazebo_request = UpdateObjectGazeboRequest()
            update_object_gazebo_request.object_name = object_name
            update_object_gazebo_request.object_pose_array = object_pose_array
            update_object_gazebo_request.object_model_name = object_model_name
            update_object_gazebo_response = update_object_gazebo_proxy(update_object_gazebo_request) 
            #print update_object_gazebo_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service update_gazebo_object is executed %s.'%str(update_object_gazebo_response))
        return update_object_gazebo_response.success

    def move_gazebo_object_client(self, object_model_name, object_pose_stamped):
        '''
            Client to move an object to a new pose in Gazebo.
        '''
        rospy.loginfo('Waiting for service move_gazebo_object.')
        rospy.wait_for_service('move_gazebo_object')
        rospy.loginfo('Calling service move_gazebo_object.')
        try:
            move_object_gazebo_proxy = rospy.ServiceProxy('move_gazebo_object', MoveObjectGazebo)
            move_object_gazebo_request = MoveObjectGazeboRequest()
            move_object_gazebo_request.object_pose_stamped = object_pose_stamped
            move_object_gazebo_request.object_model_name = object_model_name
            move_object_gazebo_response = move_object_gazebo_proxy(move_object_gazebo_request) 
            #print move_object_gazebo_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service move_gazebo_object call failed: %s'%e)
        rospy.loginfo('Service move_gazebo_object is executed %s.'%str(move_object_gazebo_response))
        return move_object_gazebo_response.success


    def add_object_blensor_client(self, object_mesh_path, object_pose, object_model_name):
        '''
            Client to add object into blensor.
        '''
        rospy.loginfo('Waiting for service /blensor/add_object.')
        rospy.wait_for_service('/blensor/add_object')
        rospy.loginfo('Calling service /blensor/add_object.')
        try:
            add_object_blensor_proxy = rospy.ServiceProxy('/blensor/add_object', AddObject)
            add_object_blensor_request = AddObjectRequest()
            add_object_blensor_request.object_name = object_model_name
            add_object_blensor_request.object_pose = object_pose
            obj_ort_quaternion = (object_pose.pose.orientation.x, object_pose.pose.orientation.y,
                             object_pose.pose.orientation.z, object_pose.pose.orientation.w) 
            obj_ort_euler = tf.transformations.euler_from_quaternion(obj_ort_quaternion)
            add_object_blensor_request.object_ort_euler = list(obj_ort_euler)
            add_object_blensor_request.path_to_mesh = object_mesh_path
            add_object_blensor_response = add_object_blensor_proxy(add_object_blensor_request) 
            #print add_object_blensor_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service add_object_blensor call failed: %s'%e)
        rospy.loginfo('Service add_object_blensor is executed %s.'%str(add_object_blensor_response))
        return add_object_blensor_response.result

    def delete_object_blensor_client(self, prev_object_model_name):
        '''
            Client to delete object from blensor.
        '''
        if prev_object_model_name is None:
            rospy.loginfo('No object to delete in blensor.')
            return

        rospy.loginfo('Waiting for service /blensor/del_object.')
        rospy.wait_for_service('/blensor/del_object')
        rospy.loginfo('Calling service /blensor/del_object.')
        try:
            del_object_blensor_proxy = rospy.ServiceProxy('/blensor/del_object', DelObject)
            del_object_blensor_request = DelObjectRequest()
            del_object_blensor_request.object_name = prev_object_model_name
            del_object_blensor_response = del_object_blensor_proxy(del_object_blensor_request) 
            #print del_object_blensor_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service del_object_blensor call failed: %s'%e)
        rospy.loginfo('Service del_object_blensor is executed %s.'%str(del_object_blensor_response))
        return del_object_blensor_response.result

    def move_object_blensor_client(self, object_pose, object_model_name):
        '''
            Client to move object pose in blender.
        '''
        rospy.loginfo('Waiting for service /blensor/move_object.')
        rospy.wait_for_service('/blensor/move_object')
        rospy.loginfo('Calling service /blensor/move_object.')
        try:
            move_object_blensor_proxy = rospy.ServiceProxy('/blensor/move_object', MoveObject)
            move_object_blensor_request = MoveObjectRequest()
            move_object_blensor_request.object_name = object_model_name
            move_object_blensor_request.object_pose = object_pose
            obj_ort_quaternion = (object_pose.pose.orientation.x, object_pose.pose.orientation.y,
                             object_pose.pose.orientation.z, object_pose.pose.orientation.w) 
            obj_ort_euler = tf.transformations.euler_from_quaternion(obj_ort_quaternion)
            move_object_blensor_request.object_ort_euler = list(obj_ort_euler)
            move_object_blensor_response = move_object_blensor_proxy(move_object_blensor_request) 
            #print move_object_blensor_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service move_object_blensor call failed: %s'%e)
        rospy.loginfo('Service move_object_blensor is executed %s.'%str(move_object_blensor_response))
        return move_object_blensor_response.result

    def manage_blensor_scene_object_client(self, object_mesh_path, object_pose, object_model_name, prev_object_model_name):
        '''
            Blensor management client to send request to create one new object and delete the 
            previous object.
            Blensor scene should be synchronized with Gazebo.
        '''
        self.delete_object_blensor_client(prev_object_model_name)
        self.add_object_blensor_client(object_mesh_path, object_pose, object_model_name)

    def get_blensor_data_client(self, camera_name='Camera'):
        '''
            Client to get blensor outputs: rgb, depth and point cloud.
        '''
        rospy.loginfo('Waiting for service /blensor/get_blensor_data.')
        rospy.wait_for_service('/blensor/get_blensor_data')
        rospy.loginfo('Calling service /blensor/get_blensor_data.')
        try:
            get_blensor_data_proxy = rospy.ServiceProxy('/blensor/get_blensor_data', GetBlensorData)
            get_blensor_data_request = GetBlensorDataRequest()
            get_blensor_data_request.camera_name = camera_name

            get_blensor_data_request.path_to_save_pcd = self.save_visual_data_pre_path + \
                    'pcd/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                    '_grasp_' + str(self.grasp_id) + '.pcd' 
            get_blensor_data_request.path_to_save_pcd_rgb = self.save_visual_data_pre_path + \
                    'rgb_image/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                    '_grasp_' + str(self.grasp_id) + '.ppm' 
            get_blensor_data_request.path_to_save_depth = self.save_visual_data_pre_path + \
                    'depth_image/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
                    '_grasp_' + str(self.grasp_id) + '.ppm' 
            #get_blensor_data_request.path_to_save_render_rgb = self.save_visual_data_pre_path + \
            #        'render_rgb_image/' + 'object_' + str(self.cur_object_id) + '_' + str(self.object_name) + \
            #        '_grasp_' + str(self.grasp_id) + '.ppm' 

            self.path_to_save_rgb = get_blensor_data_request.path_to_save_pcd_rgb
            self.path_to_save_depth = get_blensor_data_request.path_to_save_depth

            #get_blensor_data_request.path_to_save_render_rgb = '/home/kai/render_rgb.ppm'
            #get_blensor_data_request.path_to_save_pcd_rgb = '/home/kai/rgb.ppm'
            #get_blensor_data_request.path_to_save_depth = '/home/kai/depth.ppm'
            #get_blensor_data_request.path_to_save_pcd = '/home/kai/object_cloud.pcd'

            self.get_blensor_data_response = get_blensor_data_proxy(get_blensor_data_request) 
            #print get_blensor_data_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service get_blensor_data call failed: %s'%e)

        #rospy.loginfo('Service get_blensor_data is executed %s.'%str(get_blensor_data_response))
        #return get_blensor_data_response.result

    def get_pose_stamped_from_array(self, pose_array, frame_id='/world'):
        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = frame_id 
        # RPY to quaternion
        pose_quaternion = tf.transformations.quaternion_from_euler(pose_array[0], pose_array[1], pose_array[2])
        pose_stamped.pose.orientation.x, pose_stamped.pose.orientation.y, \
                pose_stamped.pose.orientation.z, pose_stamped.pose.orientation.w = pose_quaternion 
        pose_stamped.pose.position.x, pose_stamped.pose.position.y, pose_stamped.pose.position.z = \
                pose_array[3:]
        return pose_stamped

        #homo_point = self.proj_mat * point

    def project_by_blensor_calibrated_cam_info(self, pnt):
        point = np.array(pnt + [1])
        #camera_info_k_mat = np.array([526.60717328, 0.00000000, 318.52510740, 
        #    0.00000000, 526.60717328, 241.18145973, 0.00000000, 0.00000000, 1.00000000])
        #proj_mat = np.reshape(camera_info_k_mat, (3, 3))
        #proj_mat = np.array([[ 227.87088912, -32.10889887, -12.03971101, 252.59023593],
        #     [-1.0098366, 266.2641437, -33.63393949, 165.00193609],
        #      [0.,            0.,            0.,           1.        ]])
        proj_mat = np.array([-4.73/0.0078, 0., 320., 45.4807685587,
                             0., -4.73/0.0078, 240., 0.,
                            0., 0., 1., 0.])
        proj_mat = proj_mat.reshape((3,4))

        homo_point = np.matmul(proj_mat, point)
        rospy.loginfo(homo_point)
        # Convert projection matrix parameters from mm to meters.
        #homo_point *= 0.001
        proj_x = int(homo_point[0] / homo_point[2])
        proj_y = int(homo_point[1] / homo_point[2])
        # Blensor image origin is the left bottom instead of left top
        return np.array([proj_x, 480 - proj_y])

    def gen_object_pose(self):
        place_x_loc = np.random.uniform(self.place_x_min, self.place_x_max) 
        place_y_loc = np.random.uniform(self.place_y_min, self.place_y_max) 
        z_orientation = np.random.uniform(0., 2 * np.pi)
        object_pose = [0., 0., z_orientation, place_x_loc, place_y_loc, self.table_len_z]
        rospy.loginfo('Generated random object pose:')
        rospy.loginfo(object_pose)
        object_pose_stamped = dc_client.get_pose_stamped_from_array(object_pose) 
        rospy.loginfo(object_pose_stamped)
        return object_pose_stamped

    def launch_blensor_client(self, kill_blensor=False):
        rospy.loginfo('Waiting for service launch_blensor.')
        rospy.wait_for_service('launch_blensor')
        rospy.loginfo('Calling service launch_blensor.')
        try:
            launch_blensor_proxy = rospy.ServiceProxy('launch_blensor', LaunchBlensor)
            launch_blensor_request = LaunchBlensorRequest()
            if kill_blensor:
                launch_blensor_request.kill_blensor = True
            else:
                launch_blensor_request.launch_blensor = True
            self.launch_blensor_response = launch_blensor_proxy(launch_blensor_request) 
            #print self.launch_blensor_response
        except rospy.ServiceException, e:
            rospy.loginfo('Service launch_blensor call failed: %s'%e)
        rospy.loginfo('Service launch_blensor is executed %s.' %str(self.launch_blensor_response))

def test_gazebo_management_client():
    dc_client = GraspDataCollectionClient()
    dataset_dir = '/media/kai/sim_dataset/BigBird/BigBird_mesh'
    object_mesh_dirs = os.listdir(dataset_dir)
    #object_pose = [0., np.pi*0.5, np.pi*0.5, 0., -0.75, 0.4]
    #object_pose = [0., 0., 0., 0., -0.75, 0.4]
    object_pose = [0., 0., 0., 0., 0., 0.]
    
    #object_pose_stamped = dc_client.get_pose_stamped_from_array(object_pose) 

    #object_point_stamped = PointStamped()
    #object_point_stamped.header.frame_id = '/world'
    #object_point_stamped.point.x, object_point_stamped.point.y, object_point_stamped.point.z = \
    #        object_pose[3:]

    #object_move_pose = [0., 0., 0., 0.1, -0.75, 0.4]
    #object_move_pose_stamped = dc_client.get_pose_stamped_from_array(object_move_pose) 

    #listen = False
    #dc_client.listener.waitForTransform("/world", "/blensor_camera", rospy.Time(), rospy.Duration(4.0))
    #while not listen:
    #    try:
    #        dc_client.listener.waitForTransform("/world", "/blensor_camera", rospy.Time().now(), rospy.Duration(4.0))
    #        object_point_blensor_cam_stamped = dc_client.listener.transformPoint('/blensor_camera', object_point_stamped)
    #        listen = True
    #    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    #        continue
    #rospy.loginfo(object_point_blensor_cam_stamped)

    #object_point_blensor_cam = [object_point_blensor_cam_stamped.point.x, 
    #                            object_point_blensor_cam_stamped.point.y,
    #                            object_point_blensor_cam_stamped.point.z]
    ##object_point_blensor_cam = [-0.66279399, 0.56924599, -1.92847395]
    ##pixel_loc = dc_client.project_by_real_kinect_cam_info(object_pose[3:])
    #pixel_loc = dc_client.project_by_blensor_calibrated_cam_info(object_point_blensor_cam)
    #rospy.loginfo(pixel_loc)

    #object_point_blensor_cam_stamped.point.x, object_point_blensor_cam_stamped.point.y, object_point_blensor_cam_stamped.point.z = \
    #        object_point_blensor_cam
    #dc_client.get_pixel_loc_from_point_client('Camera', object_point_blensor_cam_stamped)
    #
    #return

    prev_obj_model_name = None
    #failure_obj_names = ['nutrigrain_blueberry']
    for i, obj_name in enumerate(object_mesh_dirs):
        #if obj_name in failure_obj_names:
        #    continue
        dc_client.update_gazebo_object_client(obj_name, object_pose, obj_name)
        #obj_mesh_path = dataset_dir + '/' + obj_name + '/textured_meshes/optimized_tsdf_textured_mesh.ply'
        #dc_client.manage_blensor_scene_object_client(obj_mesh_path, object_pose_stamped, obj_name, prev_obj_model_name)
        prev_obj_model_name = obj_name
        #rospy.sleep(2)
        #dc_client.move_object_blensor_client(object_move_pose_stamped, obj_name)
        #dc_client.get_blensor_data_client('Camera')
        #dc_client.get_pixel_loc_from_point_client('Camera', object_point_stamped)
        #dc_client.get_pixel_loc_from_point_client('Camera', object_point_blensor_cam_stamped)
        #rospy.sleep(1)
    #rgb = cv2.imread('/home/kai/rgb.ppm', cv2.IMREAD_COLOR)
    #cv2.circle(rgb, tuple(np.array(dc_client.get_pixel_loc_from_point_response.pixel_loc).astype(int)), radius=3, color=(0, 0, 255), thickness=2)
    #cv2.imwrite('/home/kai/rgb_plot.png', rgb)
    #render_rgb = cv2.imread('/home/kai/render_rgb.ppm', cv2.IMREAD_COLOR)
    #cv2.circle(render_rgb, tuple(np.array(dc_client.get_pixel_loc_from_point_response.pixel_loc).astype(int)), radius=3, color=(0, 0, 255), thickness=2)
    #cv2.imwrite('/home/kai/render_rgb_plot.png', render_rgb)

    #rgb = cv2.imread('/home/kai/rgb.ppm', cv2.IMREAD_COLOR)
    #cv2.circle(rgb, tuple(pixel_loc.astype(int)), radius=3, color=(0, 0, 255), thickness=2)
    #cv2.imwrite('/home/kai/rgb_plot.png', rgb)
    #render_rgb = cv2.imread('/home/kai/render_rgb.ppm', cv2.IMREAD_COLOR)
    #cv2.circle(render_rgb, tuple(pixel_loc.astype(int)), radius=3, color=(0, 0, 255), thickness=2)
    #cv2.imwrite('/home/kai/render_rgb_plot.png', render_rgb)


if __name__ == '__main__':
    #test_gazebo_management_client()
    dc_client = GraspDataCollectionClient()
    #dataset_dir = '/media/kai/sim_dataset/BigBird/BigBird_mesh'
    #object_mesh_dirs = os.listdir(dataset_dir)
    exp_part_num = 3
    dataset_dir = '/media/kai/sim_dataset/BigBird/BigBird_exp_mesh/part' + str(exp_part_num)
    #dataset_dir = '/media/kai/sim_dataset/BigBird/BigBird_exp_mesh/part_all'
    #dataset_dir = '/media/kai/sim_dataset/BigBird/BigBird_exp_mesh/part_all_2'
    object_mesh_dirs = os.listdir(dataset_dir)
    # Reverse object mesh directory list
    #object_mesh_dirs = object_mesh_dirs[::-1]
    print object_mesh_dirs

    # Bigbird objects that can not be added to moveit scene.
    # ply of colgate_cool_mint doesn't work, but seems stl works.
    prev_obj_model_name = None
    skip_object = True
    pose_failure_retry_times = 50 * dc_client.num_poses_per_object
    grasp_inits_num = 3
    grasp_inits_inf_num = grasp_inits_num + 1
    for i, object_name in enumerate(object_mesh_dirs):
        rospy.loginfo('Object: %s' %object_name)
        dc_client.get_last_object_id_name()
        # Resume from the last object.
        if dc_client.last_object_name == 'empty' or object_name == dc_client.last_object_name:
            skip_object = False
        if skip_object:
            continue

        #object_mesh_path = dataset_dir + '/' + object_name + '/textured_meshes/optimized_tsdf_textured_mesh.stl'
        object_tsdf_mesh_path = dataset_dir + '/' + object_name + '/textured_meshes/optimized_tsdf_textured_mesh'
        #object_poisson_mesh_path = dataset_dir + '/' + object_name + '/textured_meshes/optimized_poisson_textured_mesh'
        object_pose_array = [0., 0., 0., 0., -0.75, 0.4]
        object_pose_stamped = dc_client.get_pose_stamped_from_array(object_pose_array) 
        dc_client.update_gazebo_object_client(object_name, object_pose_array, object_model_name=object_name)

        grasp_id = 0
        obj_pose_id = 0
        obj_pose_failures_num = 0
        while obj_pose_id < dc_client.num_poses_per_object:
            start_time = time.time()
            rospy.loginfo('Grasp_id: %s' %str(grasp_id))

            # Use the tsdf mesh for moveit, since the poission mesh doesn't work for moveit.
            #dc_client.set_up_object_name(object_name=object_name, object_mesh_path=object_poisson_mesh_path + '.stl')
            dc_client.set_up_object_name(object_name=object_name, object_mesh_path=object_tsdf_mesh_path + '.stl')
            dc_client.set_up_grasp_id(grasp_id)

            object_pose_stamped = dc_client.gen_object_pose() 
            dc_client.move_gazebo_object_client(object_model_name=object_name, object_pose_stamped=object_pose_stamped)
            dc_client.set_up_object_pose(object_pose_stamped)

            dc_client.launch_blensor_client()
            #dc_client.manage_blensor_scene_object_client(object_poisson_mesh_path + '.ply', object_pose_stamped, object_name, prev_obj_model_name)
            dc_client.manage_blensor_scene_object_client(object_tsdf_mesh_path + '.ply', object_pose_stamped, object_name, prev_obj_model_name)
            #prev_obj_model_name = object_name

            #dc_client.move_object_blensor_client(object_pose_stamped, object_name)
    
            object_found = dc_client.segment_and_generate_preshape()
            if not object_found:
                obj_pose_failures_num += 1 
                dc_client.launch_blensor_client(kill_blensor=True)
                continue

            dc_client.launch_blensor_client(kill_blensor=True)
            
            if not dc_client.sampling_grasp:
                #Make sure inference grasp id mods total grasps per object pose is zero
                grasp_id_mod = grasp_id % grasp_inits_inf_num
                if grasp_id_mod != 0:
                    grasp_id += grasp_inits_inf_num - grasp_id_mod 

            for i in xrange(len(dc_client.preshape_response.allegro_joint_state)):
                dc_client.set_up_grasp_id(grasp_id)
                if i > 0:
                    dc_client.move_gazebo_object_client(object_model_name=object_name, object_pose_stamped=object_pose_stamped)
                grasp_arm_plan = dc_client.grasp_and_lift_object_steps(object_pose_stamped, i)
                if not grasp_arm_plan:
                    rospy.logerr('Can not find moveit plan to grasp. Ignore this grasp.\n')
                    if i == 0:
                        rospy.logerr('Can not find moveit plan for inference grasp. Ignore this object pose.\n')
                        dc_client.place_object_steps(move_arm=grasp_arm_plan)
                        break
                    obj_pose_failures_num += 1
                    if obj_pose_failures_num >= pose_failure_retry_times:
                        rospy.loginfo('Reached the maximum retry times due to planning failures!')
                        obj_pose_id = dc_client.num_poses_per_object

                    dc_client.place_object_steps(move_arm=grasp_arm_plan)
                    continue
                else:
                    if i == 0:
                        obj_pose_id += 1

                keep_current_grasp = dc_client.record_grasp_data_client(i)
                #keep_current_grasp = True
                #keep_current_grasp = False
                if keep_current_grasp:
                    grasp_id += 1
                    # Code to save patches
                    #dc_client.grasp_inference_client(i)

                dc_client.place_object_steps(move_arm=grasp_arm_plan)

                #print 'grasp_id:', grasp_id
                #while True:
                #    placement_str = raw_input('### Please manually place the object if robotic placement fails.' + \
                #            ' Type r (ready) to continue. \n')
                #    if placement_str == 'r':
                #        break
                #    else:
                #        rospy.logerr('Invalid input!')
                
            elapsed_time = time.time() - start_time
            rospy.loginfo('Time: %s' %str(elapsed_time))

        rospy.loginfo('All grasps are finished for object: %s' %object_name) 
        #break


