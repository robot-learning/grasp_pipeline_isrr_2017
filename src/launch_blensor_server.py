#!/usr/bin/env python

import roslib; roslib.load_manifest('grasp_data_collection_pkg')
import rospy
from grasp_data_collection_pkg import srv
from grasp_data_collection_pkg.srv import *
import os
import subprocess

def handle_launch_blensor(req):
    if req.launch_blensor:
        #launch_blensor_cmd = 'roslaunch blensor_ros blensor.launch'
        #os.system(launch_blensor_cmd)
        launch_blensor_cmd = ['roslaunch', 'blensor_ros', 'blensor.launch']
        subprocess.Popen(launch_blensor_cmd)
        rospy.sleep(5)
        rospy.loginfo('blensor launched.')
    elif req.kill_blensor:
        kill_blensor_cmd = 'pkill blender'
        os.system(kill_blensor_cmd)
        rospy.loginfo('Blensor killed.')

    response = LaunchBlensorResponse()
    response.success = True
    return response

def launch_blensor_server():
    launch_service = rospy.Service('launch_blensor', LaunchBlensor, handle_launch_blensor)
    rospy.loginfo('Ready to launch blensor.')
    #rospy.spin()

#def handle_kill_blensor(req):
#    if req.kill_blensor:
#        kill_blensor_cmd = 'pkill blender'
#        os.system(kill_blensor_cmd)
#        rospy.loginfo('Blender killed.')
#
#def kill_blensor_server():
#    kill_service = rospy.Service('kill_blensor', LaunchBlensor, handle_kill_blensor)
#    rospy.loginfo('Ready to kill blensor.')

if __name__ == '__main__':
    rospy.init_node('launch_blensor_server')
    launch_blensor_server()
    #kill_blensor_server()
    rospy.spin()
