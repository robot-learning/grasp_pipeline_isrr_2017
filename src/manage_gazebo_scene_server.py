#!/usr/bin/env python
import os
import roslib; roslib.load_manifest('grasp_data_collection_pkg')
import rospy
from geometry_msgs.msg import PoseStamped
from gazebo_msgs.msg import ModelStates
from grasp_data_collection_pkg.srv import *

class ManageSceneInGazebo:
    def __init__(self):
        rospy.init_node('manage_gazebo_scene_node')
        #self.save_urdf_path = save_urdf_path
        self.save_urdf_path = rospy.get_param('~save_urdf_path', '')
        self.object_meshes_path = rospy.get_param('~object_meshes_path', '')
        self.cur_urdf_path = None
        self.moved_away_objects_num = 0
        self.move_away_obj_x_dist = 10.
        self.move_away_obj_y_loc = 1000.
        self.prev_obj_model_name = None

    def get_object_mass_and_inertia(self, object_name):
        '''
            Get the mass and inertia of given object mesh.
        '''
        object_mass = 0.2
        #object_mass = 1.
        object_inertia = [1., 0., 0., 1., 0., 1.]
        return object_mass, object_inertia

    def generate_urdf(self, object_name, object_pose):
        '''
            Generate the urdf for a given object mesh with the pose, mass and inertia as
            parameters.
        '''
        object_mass, object_inertia = self.get_object_mass_and_inertia(object_name)
        #if os.access(self.object_meshes_path+"/urdf", os.F_OK) == False:
        #	try:
        #		os.mkdir(self.object_meshes_path+ "/urdf")
        #	except OSError, mkdir_error:
        #		print "Failed to create folder %s. Error message: %s" % (self.object_meshes_path+"/urdf", str(mkdir_error))
        #		exit(1)
        self.cur_urdf_path = self.save_urdf_path + '/' + object_name + '.urdf'
        f = open(self.cur_urdf_path, 'w')
        object_rpy = str(object_pose[0]) + ' ' + str(object_pose[1]) + ' ' + str(object_pose[2])
        object_location = str(object_pose[3]) + ' ' + str(object_pose[4]) + ' ' + str(object_pose[5])
        #<mu1>0.3</mu1>
        #<mu2>0.3</mu2>
        urdf_str = """
    <robot name=\"""" + object_name + """\">
      <link name=\"""" + object_name + """_link">
        <inertial>
          <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
          <mass value=\"""" + str(object_mass) + """\" />
          <inertia  ixx=\"""" + str(object_inertia[0]) + """\" ixy=\"""" + str(object_inertia[1]) + """\"  ixz=\"""" + \
                  str(object_inertia[2]) + """\"  iyy=\"""" + str(object_inertia[3]) + """\"  iyz=\"""" + str(object_inertia[4]) + \
                  """\"  izz=\"""" + str(object_inertia[5]) + """\" />
        </inertial>
        <visual>
          <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
          <geometry>
            <mesh filename=\"file://""" + self.object_meshes_path + """/""" + object_name + """/""" + """textured_meshes/optimized_tsdf_textured_mesh.stl\" />
          </geometry>
        </visual>
        <collision>
          <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
          <geometry>
            <mesh filename=\"file://""" + self.object_meshes_path + """/""" + object_name + """/""" + """textured_meshes/optimized_tsdf_textured_mesh.stl\" />
          </geometry>
        </collision>
      </link>
      <gazebo reference=\"""" + object_name + """_link\">
        <mu1>1.0</mu1>
        <mu2>1.0</mu2>
        <material>Gazebo/Red</material>
      </gazebo>
    </robot>
    """
        f.write(urdf_str)
        f.close()

    def move_object(self, object_model_name, object_pose_quart_array):
        """
            Move object to a certain pose (Quaternion for orientation).
        """
        move_obj_cmd = "rosservice call /gazebo/set_model_state " + "'{model_state: { model_name: " + object_model_name + \
                            ", pose: { position: { x: " + str(object_pose_quart_array[4]) + ", y: " + str(object_pose_quart_array[5]) + \
                            ", z: "+ str(object_pose_quart_array[6]) + " }, orientation: {x: " + str(object_pose_quart_array[0]) + \
                            ", y: " + str(object_pose_quart_array[1]) + ", z: " + str(object_pose_quart_array[2]) + ", w: " + \
                            str(object_pose_quart_array[3]) + " } }, reference_frame: world } }'"
        print 'move_obj_cmd:', move_obj_cmd
        os.system(move_obj_cmd)


    def move_away_prev_object(self):
        """
            Move the previous object to somewhere further away. This is to get around the issue that
            dart would crash after deleting objects.
        """
        if self.prev_obj_model_name is None:
            return
        #move_away_obj_cmd = "rosservice call /gazebo/set_model_state " + "'{model_state: { model_name: " + self.prev_obj_model_name + \
        #                    ", pose: { position: { x: " + str(self.move_away_obj_x_dist * self.moved_away_objects_num) + \
        #                    ", y: " + str(self.move_away_obj_y_loc) + ", z: 0 }, orientation: {x: 0, y: 0, z: 0, w: 1 } }, reference_frame: world } }'"
        ##print 'move_away_obj_cmd:', move_away_obj_cmd
        #os.system(move_away_obj_cmd)
        object_away_x_loc = self.move_away_obj_x_dist * self.moved_away_objects_num
        object_pose_quart_array = [0., 0., 0., 1., object_away_x_loc, self.move_away_obj_y_loc, 0]
        self.move_object(self.prev_obj_model_name, object_pose_quart_array)
        self.moved_away_objects_num += 1
        
    def spawn_object(self, object_name, obj_model_name, object_pose_array):
        '''
            Spawn the generated object urdf with the given object model name.
        '''
        self.cur_urdf_path = self.save_urdf_path + '/' + object_name + '.urdf'
        spawn_object_cmd = "rosrun gazebo_ros spawn_model -file " + self.cur_urdf_path + " -urdf -x " + str(object_pose_array[3]) +\
                " -y "+ str(object_pose_array[4]) + " -z " + str(object_pose_array[5]) + " -model " + obj_model_name 
        print 'spawn_object_cmd:', spawn_object_cmd
        os.system(spawn_object_cmd)
        self.prev_obj_model_name = obj_model_name
    
    def handle_update_gazebo_object(self, req):
        '''
            Gazebo scene management server handler.
            Generate the urdf for the given object mesh, spawn the urdf and delete the previous object.
            parameter: req.object_pose = [r, p, y, x, y, z]
        '''
        self.move_away_prev_object()
        mesh_pose_array = [0., 0., 0., 0., 0., 0.]
        #self.generate_urdf(req.object_name, mesh_pose_array)
        self.spawn_object(req.object_name, req.object_model_name, req.object_pose_array)

        response = UpdateObjectGazeboResponse()
        response.success = True
        return response

    def update_gazebo_object_server(self):
        rospy.Service('update_gazebo_object', UpdateObjectGazebo, self.handle_update_gazebo_object)
        rospy.loginfo('Service update_gazebo_object:')
        rospy.loginfo('Ready to update the object in gazebo scene.')

    def handle_move_gazebo_object(self, req):
        '''
            Move object to a new pose.
        '''
        object_pose_quart_array = [req.object_pose_stamped.pose.orientation.x, req.object_pose_stamped.pose.orientation.y,
                                    req.object_pose_stamped.pose.orientation.z, req.object_pose_stamped.pose.orientation.w,
                                    req.object_pose_stamped.pose.position.x, req.object_pose_stamped.pose.position.y,
                                    req.object_pose_stamped.pose.position.z]
        self.move_object(req.object_model_name, object_pose_quart_array)
        response = MoveObjectGazeboResponse()
        response.success = True
        return response

    def move_gazebo_object_server(self):
        rospy.Service('move_gazebo_object', MoveObjectGazebo, self.handle_move_gazebo_object)
        rospy.loginfo('Service move_gazebo_object:')
        rospy.loginfo('Ready to move the object in gazebo scene.')



if __name__=='__main__':
    #save_urdf_path = '/media/kai/sim_dataset/BigBird/BigBird_urdf'
    #manage_gazebo_scene = ManageGazeboScene(save_urdf_path)
    #mesh_path = '/media/kai/sim_dataset/BigBird/BigBird_mesh'
    ##object_name = '3m_high_tack_spray_adhesive' 
    #object_name = 'campbells_soup_at_hand_creamy_tomato'
    #object_pose = [0.] * 6
    #obj_model_name = object_name + '_1'
    #manage_gazebo_scene.generate_urdf(mesh_path, object_name, object_pose)
    #manage_gazebo_scene.spawn_object(obj_model_name)
    #manage_gazebo_scene.move_away_prev_object()
    #obj_model_name = object_name + '_2'
    #manage_gazebo_scene.spawn_object(obj_model_name)
    #manage_gazebo_scene.move_away_prev_object()
    #obj_model_name = object_name + '_3'
    #manage_gazebo_scene.spawn_object(obj_model_name)
    manage_gazebo_scene = ManageSceneInGazebo()
    manage_gazebo_scene.update_gazebo_object_server()
    manage_gazebo_scene.move_gazebo_object_server()
    rospy.spin()



